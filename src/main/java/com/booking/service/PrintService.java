package com.booking.service;

import java.lang.reflect.Member;
import java.util.List;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Membership;
import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;


public class PrintService {
    public static void printMenu(String title, String[] menuArr){
        int num = 1;
        System.out.println(title);
        for (int i = 0; i < menuArr.length; i++) {
            if (i == (menuArr.length - 1)) {   
                num = 0;
            }
            System.out.println(num + ". " + menuArr[i]);   
            num++;
        }
    }

    public static String printServices(List<Service> serviceList){
        String result = "";
        // Bisa disesuaikan kembali
        for (Service service : serviceList) {
            result += service.getServiceName() + ", ";
        }
        return result;
    }
    
    public static void showAllService(List<Service> serviceList) {
    	int num = 1;
    	System.out.printf("| %-4s | %-8s | %-20s | %-15s |\n",
                "No.", "ID", "Nama Service", "Harga");
        System.out.println("+===========================================================================================+");
		for (Service service : serviceList) {
        	System.out.printf("| %-4s | %-8s | %-20s | %-15s |\n",
                    num, service.getServiceId(), service.getServiceName(), service.getPrice());
                    num++;
        	}
        }
    
    
    // Function yang dibuat hanya sebgai contoh bisa disesuaikan kembali
    public static void showRecentReservation(List<Reservation> reservationList){
        int num = 1;
        System.out.printf("| %-4s | %-7s | %-15s | %-15s | %-15s | %-15s | %-10s |\n",
                "No.", "ID", "Nama Customer", "Pegawai", "Service", "Biaya Service", "Workstage");
        System.out.println("+========================================================================================================+");
        for (Reservation reservation : reservationList) {
            if (reservation.getWorkstage().equalsIgnoreCase("Waiting") || reservation.getWorkstage().equalsIgnoreCase("In process")) {
                System.out.printf("| %-4s | %-7s | %-15s | %-15s | %-15s | %-15s | %-10s |\n",
                num, reservation.getReservationId(), reservation.getCustomer().getName(), reservation.getEmployee().getName(), printServices(reservation.getServices()), reservation.getReservationPrice(), reservation.getWorkstage());
                num++;
            }
        }
    }

    public static void showAllCustomer(List<Person> personList){
    	int num = 1;
        System.out.printf("| %-4s | %-8s | %-15s | %-15s | %-15s | %-15s |\n",
                "No.", "ID", "Nama Customer", "Alamat", "Membership", "Uang");
        System.out.println("+===========================================================================================+");
		for (Person person : personList) {
        	if (person instanceof Customer) {
				Customer customer = (Customer)person; 
        	System.out.printf("| %-4s | %-8s | %-15s | %-15s | %-15s | %-15s |\n",
                    num, person.getId(), person.getName(), person.getAddress(), ((Customer) person).getMember().getMembershipName(), ((Customer) person).getWallet());
                    num++;
        	}
        }
    }

    public static void showAllEmployee(List<Person> personList){
    	int num = 1;
        System.out.printf("| %-4s | %-8s | %-11s | %-15s | %-15s |\n",
                "No.", "ID", "Nama", "Alamat", "Pengalaman");
        System.out.println("+==========================================================================================+");
        int experience = 0;
        for (Person person : personList) {
        	if (person instanceof Employee) {
        		Employee employee = (Employee)person;
        		experience = ((Employee)person).getExperience();
        	 
                System.out.printf("| %-4s | %-8s | %-11s | %-15s | %-15s |\n",
                num, person.getId(), person.getName(), person.getAddress(), experience);
                num++;
        	}
        }
    }

    public static void showHistoryReservation(List<Reservation> reservationList){
    	int num = 1;
        System.out.printf("| %-4s | %-7s | %-15s | %-15s | %-15s | %-15s |\n",
                "No.", "ID", "Nama Customer", "Service", "Total Biaya", "Workstage");
        System.out.println("+========================================================================================================+");
        for (Reservation reservation : reservationList) {
            //if (reservation.getWorkstage().equalsIgnoreCase("Cancel") || reservation.getWorkstage().equalsIgnoreCase("Finish")) {
        	System.out.printf("| %-4s | %-7s | %-15s | %-15s | %-15s | %-15s |\n",
                num, reservation.getReservationId(), reservation.getCustomer().getName(), printServices(reservation.getServices()), reservation.getReservationPrice(), reservation.getWorkstage());
                num++;
            //}
        }
    }
}
