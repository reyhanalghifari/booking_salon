package com.booking.service;

import java.util.List;

import com.booking.models.Reservation;

public class ValidationService {
    // Buatlah function sesuai dengan kebutuhan
	public static int idReservasi = 1;
	
    public static void validateInput(){

    }

    public static void validateCustomerId(){

    }
    
    public static String formatIdNumber(int numberId) {
		if(numberId < 10) {
			return "0" + numberId;
		}else if(numberId < 100) {
			return "0" + numberId;
		}else {
			return Integer.toString(numberId);
		}
	}
	
	public static String getIdReservasi( List<Reservation> reservationList, String reservationId) {
		String resultString = "";
		String formatId = "";
		for(int i=0; i<reservationList.size();i++) {
		if(reservationId.equalsIgnoreCase(reservationList.get(i).getReservationId())) {
			resultString = formatIdNumber(idReservasi);
			idReservasi++;
		}
		}
		return formatId+resultString;
	}
	
	public static String generateResIdString (int reservationId) {
		String resultString ;
		
		if (reservationId >= 100) {
			resultString = "Rsv-" + reservationId;
		} else if (reservationId >= 10) {
			resultString = "Rsv-0" + reservationId;
		} else {
			resultString = "Rsv-0" + reservationId;
		}
		
		return resultString;
	}
}
