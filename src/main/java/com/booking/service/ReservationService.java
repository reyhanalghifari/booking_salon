package com.booking.service;

import java.util.ArrayList;
import java.util.List;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;

public class ReservationService {
	
	public static Reservation createReservation(List<Service> serviceList, List<Person>personList, List<Reservation> reservationList, int reservationId, String customerId, String employeeId, List<Service> services,
            String workstage){
		List<String> serviceId = new ArrayList<>();
		List<Service> serviceData = new ArrayList<>();
		for(String service : serviceId) {
			serviceData.add(ReservationService.getServiceByServiceId(serviceList, service));
		}
		//Service service = getServiceByServiceId(serviceList, serviceId);
		Customer customer = getCustomerByCustomerId(personList, customerId);
    	Employee employee = getEmployeeByEmployeeId(personList, employeeId);
    	
    	Reservation reservation = new Reservation(ValidationService.generateResIdString(reservationId), customer, employee, serviceData, 0, workstage);
    	reservationList.add(reservation);
    	
    	return reservation;
    }
	
	public static List<Reservation> getAllData(List<Reservation> reservationList){
		List<Reservation> result = new ArrayList<>();
		for(Reservation reservation : reservationList) {
			if(!result.contains(reservation)) {
				result.add(reservation);
			}
		}
		return result;
	}
    	
    public static Customer getCustomerByCustomerId(List<Person>personList, String id){
        return personList.stream()
        		.filter(person -> person instanceof Customer)
        		.map(customer -> (Customer)customer)
        		.filter(customer -> customer.getId().equalsIgnoreCase(id))
        		.findFirst()
        		.orElse(null);
    }
   
    public static Employee getEmployeeByEmployeeId(List<Person>personList, String id){
    	return personList.stream()
        		.filter(person -> person instanceof Employee)
        		.map(employee -> (Employee)employee)
        		.filter(employee -> employee.getId().equalsIgnoreCase(id))
        		.findFirst()
        		.orElse(null);
    }
    
//    public static Service getServiceByServiceId(List<Service> serviceList, String serviceId){
//    	int result = 0;
//    	for(int i=0; i<serviceList.size(); i++) {
//    		if(serviceId.equalsIgnoreCase(serviceList.get(i).getServiceId())) {
//        		result = i;
//        		break;
//        	}
//        }
//		return serviceList.get(result);
//    }
    public static Service getServiceByServiceId(List<Service> serviceList, String serviceId){
    	return serviceList.stream()
        		.filter(service -> service.getServiceId().equalsIgnoreCase(serviceId))
        		.findFirst()
        		.orElse(null);
    }
    
    public static void editReservationWorkstage(List<Reservation>reservationList, String reservationId, String workstage){
        for(int i=0; i<reservationList.size();i++) {
        	if(reservationId.equalsIgnoreCase(reservationList.get(i).getReservationId())) {
        		Reservation reservation = new Reservation(reservationId, reservationList.get(i).getCustomer(), reservationList.get(i).getEmployee(), reservationList.get(i).getServices(), 0, workstage);
        	reservationList.set(i, reservation);
        	break;
        	}
        }
    }

    // Silahkan tambahkan function lain, dan ubah function diatas sesuai kebutuhan
}
